from collections import OrderedDict

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

class TestContacts(APITestCase):

    def setUp(self):
        self.user1 = User.objects.create_user(username='testuser1', password='12345')
        self.user2 = User.objects.create_user(username='testuser2', password='12345')

        self.client1 = APIClient()
        self.client1.login(username='testuser1', password='12345')

        self.client2 = APIClient()
        self.client2.login(username='testuser2', password='12345')

        self.client3 = APIClient()

    def test_create_contacts(self):
        response = self.client3.post(reverse('contact-list'))
        self.assertEquals(response.status_code, 403)

        response = self.client2.post(reverse('contact-list'))
        self.assertEquals(response.status_code, 400)

        data = {
            'email': 'me@manan.xyz',
            'name': 'Manan Shah',
            'phone': '1233211231'
        }
        response = self.client2.post(reverse('contact-list'), data)
        self.assertEquals(response.status_code, 201)
        self.assertEquals(response.data['email'], data['email'])
        self.assertEquals(response.data['name'], data['name'])
        self.assertEquals(response.data['phone'], data['phone'])
        self.assertEquals('id' in response.data, True)

        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 1)
        self.assertEquals(response.data['results'], [OrderedDict(
            id=1,
            url='http://testserver/contact/1/',
            name='Manan Shah',
            email='me@manan.xyz',
            phone='1233211231',
            user='http://testserver/user/1/'
        )])

        data = {
            'email': 'me@manan.xyz',
            'name': 'Manan Test',
            'phone': '1233211231'
        }
        response = self.client2.post(reverse('contact-list'), data)
        self.assertEquals(response.status_code, 400)

        response = self.client1.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 0)

    def test_edit_contacts(self):
        data = {
            'email': 'me@manan.xyz',
            'name': 'Manan Shah',
            'phone': '1233211231'
        }
        response = self.client2.post(reverse('contact-list'), data)

        id = response.data['id']

        response = self.client2.patch(reverse('contact-detail', kwargs={'pk': id}), {
            'name': 'Manan Change Shah'
        })

        self.assertEquals(response.data['id'], id)
        self.assertEquals(response.data['name'], 'Manan Change Shah')

        response = self.client1.patch(reverse('contact-detail', kwargs={'pk': id}), {
            'name': 'Manan Change Shah'
        })
        self.assertEquals(response.status_code, 404)

        response = self.client3.patch(reverse('contact-detail', kwargs={'pk': id}), {
            'name': 'Manan Change Shah'
        })
        self.assertEquals(response.status_code, 403)

    def test_delete_contacts(self):
        data = {
            'email': 'me@manan.xyz',
            'name': 'Manan Shah',
            'phone': '1233211231'
        }
        response = self.client2.post(reverse('contact-list'), data)
        id = response.data['id']

        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 1)

        response = self.client1.delete(reverse('contact-detail', kwargs={'pk': id}))
        self.assertEquals(response.status_code, 404)

        response = self.client2.delete(reverse('contact-detail', kwargs={'pk': id}))
        self.assertEquals(response.status_code, 204)

        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 0)

        response = self.client3.delete(reverse('contact-detail', kwargs={'pk': id}))
        self.assertEquals(response.status_code, 403)

    def test_list_contacts(self):

        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 0)

        data = {
            'email': 'me@manan.xyz',
            'name': 'Manan Shah',
            'phone': '1233211231'
        }
        self.client2.post(reverse('contact-list'), data)

        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 1)

        data = {
            'email': 'me@manan1.xyz',
            'name': 'Manan Shah',
            'phone': '1233211231'
        }
        self.client2.post(reverse('contact-list'), data)
        response = self.client2.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 2)

        response = self.client1.get(reverse('contact-list'))
        self.assertEquals(len(response.data['results']), 0)

        response = self.client3.get(reverse('contact-list'))
        self.assertEquals(response.status_code, 403)