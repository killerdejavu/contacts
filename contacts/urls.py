from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from contacts.views import ContactViewSet, UserViewSet

router = routers.DefaultRouter()
router.register(r'contact', ContactViewSet)
router.register(r'user', UserViewSet)

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'api-auth/', include('rest_framework.urls')),
    url(r'^', include(router.urls)),
]
