from django.contrib.auth.models import User
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from contacts.models import Contact
from contacts.permissions import IsContactOwner, IsSameUser
from contacts.serializers import ContactSerializer, UserSerializer


class ContactViewSet(ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = (IsAuthenticated, IsContactOwner,)

    def get_queryset(self):
        kwargs = {
            'user': self.request.user
        }

        name = self.request.query_params.get('name', None)
        if name:
            kwargs['name__search'] = name

        email = self.request.query_params.get('email', None)
        if email:
            kwargs['email'] = email

        return Contact.objects.filter(**kwargs)


    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsSameUser,)

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)
