from django.contrib.auth.models import User
from rest_framework import serializers

from contacts.models import Contact


class ContactSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedIdentityField(view_name='user-detail')

    class Meta:
        model = Contact
        fields = ('id', 'url', 'name', 'email', 'phone', 'user')


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'url', 'email')
