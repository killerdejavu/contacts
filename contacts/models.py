from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE


class Contact(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    email = models.EmailField(unique=True, null=False, blank=False, db_index=True)
    phone = models.CharField(max_length=10)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(to=User, on_delete=CASCADE)

    class Meta:
        ordering = ('-id', )