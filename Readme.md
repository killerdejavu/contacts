## Contacts API
- Its a REST API written in Django + DRF

## API
- Heroku Endpoint - https://your-contacts.herokuapp.com/contact/
- Name Search - https://your-contacts.herokuapp.com/contact/?name=Laura
- Email Search - https://your-contacts.herokuapp.com/contact/?email=freemandawn@gmail.com

## Pagination
- Defaults to 10 results in any list, search (list with filter) api call
- Use next and previous in the response to go to the next and previous page
- count gives the total counts for the API

## Running the server
- python manager.py runserver
- In Prod better to use "gunicorn contacts.wsgi" for faster server

## Running tests
- python manage.py test -v 2
- Testsuite is not extensive, its more indicative. Only Integrations tests written. Will update more if time permits
